package com.bsc.interview;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class PackagesRepositoryTest {

    private PackagesRepository repo;

    @Before
    public void setUp() {
        repo = new PackagesRepository();
    }

    @Test
    public void addPackage() {
        final int postCode = 12345;
        repo.addPackage(new PackageData(1.0, postCode));
        repo.addPackage(new PackageData(2.0, postCode));
        PackageData pkg = repo.getPackage(postCode);

        assertEquals(new PackageData(3.0, postCode), pkg);
    }

    @Test
    public void getPackagesShouldbeSorted() {
        repo.addPackage(new PackageData(1.0, 1));
        repo.addPackage(new PackageData(2.0, 2));
        repo.addPackage(new PackageData(3.0, 3));
        Stream<PackageData> pkgs = repo.getSortedPackages();

        List<Double> weights = pkgs.map(PackageData::getWeight).collect(Collectors.toList());
        assertEquals(Arrays.asList(3.0, 2.0, 1.0), weights);
    }
}