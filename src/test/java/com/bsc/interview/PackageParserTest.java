package com.bsc.interview;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PackageParserTest {

    private PackageParser parser;

    @Before
    public void setUp() {
        parser = new PackageParser();
    }

    @Test
    public void correctSyntax() {
        assertNotNull(parser.apply("12 12345"));
        assertNotNull(parser.apply("12.3 12345"));

        PackageData parsedPkg = parser.apply("12.345 12345");
        assertEquals(new PackageData(12.345d, 12345), parsedPkg);
    }

    @Test
    public void malformedWeight() {
        assertNull(parser.apply("12.x45 12345"));
    }

    @Test
    public void malformedPostCode() {
        assertNull(parser.apply("12.345 12x45"));
    }

    @Test
    public void postCodeTooShort() {
        assertNull(parser.apply("12.345 1234"));
    }

}