package com.bsc.interview;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * Handles command line interface and loading initial package records from given file.
 */
public class PackageRecorder implements Runnable {

    private final String initialPackagesFile;

    private final PackagesRepository repo;

    private final PackageParser parser;

    private final ExecutorService executor;

    public PackageRecorder(String initialPackagesFile, PackagesRepository repo, PackageParser parser, ExecutorService executor) {
        this.initialPackagesFile = initialPackagesFile;
        this.repo = repo;
        this.parser = parser;
        this.executor = executor;
    }

    /**
     * Actually starts package recorder.
     */
    @Override
    public void run() {
        System.out.println("Package Recorder started.");
        if (initialPackagesFile != null) {
            System.out.println("Loading initial packages from " + initialPackagesFile);
            initRepository();
            System.out.println("Initialization done.");
        }

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter packages data, 'quit' to exit.");
        System.out.println("------------------------------------");

        while (true) {
            String line = scanner.nextLine();
            if ("quit".equalsIgnoreCase(line)) {
                System.out.println("Shutting down...");
                executor.shutdown();
                break;
            }
            PackageData newPkg = parser.apply(line);
            if (newPkg != null) {
                repo.addPackage(newPkg);
                System.out.println("Added package: " + newPkg);
            }
        }
    }

    private void initRepository() {
        Path path = Paths.get(initialPackagesFile);
        try {
            Stream<String> packageLines = Files.lines(path);
            packageLines.forEach((pkgString) -> {
                PackageData pkg = parser.apply(pkgString);
                if (pkg != null) {
                    repo.addPackage(pkg);
                }
            });
            packageLines.close();
        } catch (IOException e) {
            System.err.println("Unable to load packages file '" + initialPackagesFile + "'. Error: " + e);
        }

    }


    /**
     * Main entry point, binds the dependencies and starts package recorder cli.
     *
     * @param args File name to load initial package records from
     */
    public static void main(String[] args) {
        PackagesRepository repository = new PackagesRepository();

        // start dumper in dedicated thread
        RepositoryDumper dumper = new RepositoryDumper(repository);
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
        executor.scheduleAtFixedRate(dumper, 1L, 1L, TimeUnit.MINUTES);

        // start recorder in main thread
        String initialPkgsFile = args != null && args.length > 0 ? args[0] : null;
        new PackageRecorder(initialPkgsFile, repository, new PackageParser(), executor).run();
    }

}
