package com.bsc.interview;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses string representation of the package to Package object, returns null on failure
 */
public class PackageParser implements Function<String, PackageData> {

    private static final Pattern linePattern = Pattern.compile("([0-9]+)(.[0-9]{0,3})? ([0-9]{5})");

    @Override
    public PackageData apply(String s) {
        Matcher matcher = linePattern.matcher(s);

        if (matcher.matches()) {
            String fractional = matcher.group(2);
            double weight = fractional != null
                    ? Double.parseDouble(matcher.group(1) + fractional)
                    : Double.parseDouble(matcher.group(1));
            int postCode = Integer.parseInt(matcher.group(3));
            return new PackageData(weight, postCode);
        } else {
            System.err.println("Package string '" + s + "' is malformed, correct example format: '12.345 12345'");
            return null;
        }
    }
}
