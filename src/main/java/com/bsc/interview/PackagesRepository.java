package com.bsc.interview;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * Thread safe repository of packages with aggregated weights, grouped by their post code.
 */
public class PackagesRepository {

    private final Map<Integer, PackageData> packages = new ConcurrentHashMap<>();

    /**
     * Add package weight to its post code group
     * @param pkg New package data
     * @return Package with aggregated weight after addition
     */
    public PackageData addPackage(PackageData pkg) {
        PackageData existing = packages.get(pkg.getPostCode());
        if (existing != null) {
            pkg = new PackageData(existing.getWeight() + pkg.getWeight(), pkg.getPostCode());
        }
        packages.put(pkg.getPostCode(), pkg);

        return pkg;
    }

    public PackageData getPackage(int postCode) {
        return packages.get(postCode);

    }

    /**
     * @return Packages in repository, sorted descending by package weight
     */
    public Stream<PackageData> getSortedPackages() {
        return packages.values().stream().sorted(
                (p1, p2) -> Double.compare(p2.getWeight(), p1.getWeight())
        );
    }

}
