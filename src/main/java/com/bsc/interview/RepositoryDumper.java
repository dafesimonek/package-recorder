package com.bsc.interview;

import java.time.LocalTime;

/**
 * Can print packages repository content.
 */
public class RepositoryDumper implements Runnable {

    private final PackagesRepository repo;

    public RepositoryDumper(PackagesRepository repo) {
        this.repo = repo;
    }

    /**
     * Prints packages repository content to standard output.
     */
    @Override
    public void run() {
        System.out.println("---Packages dump (time: " + LocalTime.now() + ")---");
        repo.getSortedPackages().forEach(System.out::println);
        System.out.println("---End of packages dump---");
    }
}
