package com.bsc.interview;

import java.text.DecimalFormat;
import java.util.Objects;

/**
 * Immutable package record data
 */
public class PackageData {

    private final double weight;

    private final int postCode;

    private static final DecimalFormat weightFormat = new DecimalFormat("#.000");
    private static final DecimalFormat postCodeFormat = new DecimalFormat("00000");

    public PackageData(double weight, int postCode) {
        this.weight = weight;
        this.postCode = postCode;
    }

    public double getWeight() {
        return weight;
    }

    public int getPostCode() {
        return postCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackageData aPackageData = (PackageData) o;
        return Double.compare(aPackageData.weight, weight) == 0 &&
                postCode == aPackageData.postCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, postCode);
    }

    @Override
    public String toString() {
        return "postCode: " + postCodeFormat.format(postCode) +
                " weight: " + weightFormat.format(weight);
    }

}
