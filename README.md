# Package Recorder #

Package recorder cli application based on BSC interview exercise.  

### Prerequisities ###

* `Java 8` installed, on path
* `Maven 3.x` installed, on path 

### Running ###

* Use script **`./run.sh`** from root project dir to compile and run plain console   
* Run **`./run.sh initial_packages.txt`** as an example to load packages from given file first 

### Tests ###

* Run script **`./run.sh`** or **`mvn test`** from root project dir to run tests

#### Input errors handling ####
If input from console or file is malformed or file is missing, application will 
print message to standard error stream and keep trucking.  
